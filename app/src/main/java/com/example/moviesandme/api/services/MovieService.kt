package com.example.moviesandme.api.services

import com.example.moviesandme.models.Movie
import com.example.moviesandme.models.MovieResponse
import retrofit2.Call
import retrofit2.http.*

interface MovieService {

    @GET("movie/{id}")
    fun getMovieById(
        @Path("id") id: String,
        @Query("api_key") apiKey: String = "d833b51a51f797d31b2ec5344445c62e",
        @Query("language") language: String = "fr"
    ) : Call<Movie>

    @GET("search/movie")
    fun getSearchMovie(
        @Query("query") movieName: String,
        @Query("page") page: Int,
        @Query("api_key") apiKey: String = "d833b51a51f797d31b2ec5344445c62e",
        @Query("language") language: String = "fr"
    ) : Call<MovieResponse>

    @GET("movie/upcoming")
    fun getLastMovies(
        @Query("api_key") apiKey: String = "d833b51a51f797d31b2ec5344445c62e",
        @Query("language") language: String = "fr"
    ) : Call<MovieResponse>
}