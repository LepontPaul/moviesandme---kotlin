package com.example.moviesandme.adapters

import android.app.ListActivity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesandme.R
import com.example.moviesandme.activities.MovieDetailActivity
import com.example.moviesandme.models.Movie
import com.facebook.drawee.view.SimpleDraweeView
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.simple_string_list_item.view.*

typealias MovieClicked = (Movie) -> Unit

class FavoriteAdapter(val list: MutableList<Movie>) :
    RecyclerView.Adapter<StringViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): StringViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.simple_string_list_item,
            parent,
            false
        )
        return StringViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.bind(list[position], movieClicked = {
            val index = list.indexOf(it)
            list.remove(it)
            notifyItemRemoved(index)
        })

    }
}

class StringViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(movies: Movie, movieClicked: MovieClicked) {
        view._hearthFavorite.setImageResource(R.drawable.ic_favorite_black_24dp)
        view._listVoteAverage.text = movies.vote_average.toString()
        val draweeView = view.my_image_view as SimpleDraweeView
        draweeView.setImageURI("https://image.tmdb.org/t/p/w500${movies.poster_path}")
        view.setOnClickListener{
            MovieDetailActivity.start(
                it.context,
                movies.id
            )
        }
        view._hearthFavorite.setOnClickListener {
            movies.apply {
                isInFavorites = !isInFavorites
            }.save()
            movieClicked.invoke(movies)
        }
    }


}