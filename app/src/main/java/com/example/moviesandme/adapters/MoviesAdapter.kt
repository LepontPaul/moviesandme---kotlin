package com.example.moviesandme.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesandme.R
import com.example.moviesandme.activities.MovieDetailActivity
import com.example.moviesandme.models.Movie
import com.facebook.drawee.view.SimpleDraweeView
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.simple_string_list_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MoviesAdapter(val list: List<Movie>) :
    RecyclerView.Adapter<MoviesAdapter.StringViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): StringViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            com.example.moviesandme.R.layout.simple_string_list_item,
            parent,
            false
        )
        return StringViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class StringViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movies: Movie) {
            if (movies.isInFavorites) view._hearthFavorite.setImageResource(R.drawable.ic_favorite_black_24dp) else view._hearthFavorite.setImageResource(
                R.drawable.ic_favorite_border_black_24dp
            )
            view._listVoteAverage.text = movies.vote_average.toString()
            val draweeView = view.my_image_view as SimpleDraweeView
            draweeView.setImageURI("https://image.tmdb.org/t/p/w500${movies.poster_path}")
            view.setOnClickListener{
                MovieDetailActivity.start(
                    it.context,
                    movies.id
                )
            }
            view._hearthFavorite.setOnClickListener {
                movies.apply {
                    isInFavorites = !isInFavorites
                }.save()
                bind(movies)
            }
        }
    }
}
