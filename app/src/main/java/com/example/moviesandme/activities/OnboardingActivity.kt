package com.example.moviesandme.activities

import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.moviesandme.ui.MyPaperOnboardingFragment
import com.facebook.drawee.backends.pipeline.Fresco
import com.pixplicity.easyprefs.library.Prefs
import com.ramotion.paperonboarding.PaperOnboardingFragment
import com.ramotion.paperonboarding.PaperOnboardingPage
import com.ramotion.paperonboarding.listeners.PaperOnboardingOnRightOutListener
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_onboarding.*


class OnboardingActivity : AppCompatActivity() {

    companion object {
        const val HAS_LAUNCHED_APP_ONCE = "HAS_LAUNCHED_APP_ONCE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fresco.initialize(this)
        Realm.init(this)
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()

        setContentView(com.example.moviesandme.R.layout.activity_onboarding)

        if (Prefs.contains("TODOCHANGE"/*HAS_LAUNCHED_APP_ONCE*/))
            startMain()
        else
        // onBoarding
            displayOnboarding()
    }

    private fun startMain(){
        Prefs.putBoolean(HAS_LAUNCHED_APP_ONCE, true)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun displayOnboarding() {

        val page1 = PaperOnboardingPage(
            "Movies and me",
            "Rechercher vos films favoris",
            ContextCompat.getColor(this, com.example.moviesandme.R.color.design_default_color_primary),
            com.example.moviesandme.R.drawable.ic_movie_filter_black_96dp,
            com.example.moviesandme.R.drawable.ic_movie_filter_black_96dp
        )

        val page2 = PaperOnboardingPage(
            "Favoris",
            "Ajouter vos films préférer à votre liste de favoris <3 ",
            ContextCompat.getColor(this, com.example.moviesandme.R.color.colorPrimary),
            com.example.moviesandme.R.drawable.ic_playlist_add_check_white_96dp,
            com.example.moviesandme.R.drawable.ic_playlist_add_check_white_96dp
        )

        val page3 = PaperOnboardingPage(
            "Prêt ?",
            "Having fun !",
            ContextCompat.getColor(this, com.example.moviesandme.R.color.colorAccent),
            com.example.moviesandme.R.drawable.ic_check_black_96dp,
            com.example.moviesandme.R.drawable.ic_check_black_96dp
        )

        val fragment = MyPaperOnboardingFragment.newInstance(
            arrayListOf(page1, page2, page3)
        )

        fragment.setOnRightOutListener(PaperOnboardingOnRightOutListener {
            startMain()
        })

        _buttonSkip.setOnClickListener{
            startMain()
        }

        supportFragmentManager.beginTransaction().add(com.example.moviesandme.R.id._frame, fragment)
            .commit()
    }

}
