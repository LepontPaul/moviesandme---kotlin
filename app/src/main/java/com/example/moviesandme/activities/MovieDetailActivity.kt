package com.example.moviesandme.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviesandme.R
import com.example.moviesandme.adapters.MoviesAdapter
import com.example.moviesandme.models.Movie
import com.vicpin.krealmextensions.queryFirst
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher

class MovieDetailActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"
        fun start(context: Context, movieId: Int) {
            context.startActivity(
                Intent(context, MovieDetailActivity::class.java).apply {
                    putExtra(EXTRA_MOVIE_ID, movieId)
                }
            )
        }
    }

    private val movieIdFromExtra by lazy {
        intent?.getIntExtra(EXTRA_MOVIE_ID, -1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        GlobalScope.launch {
            Movie().queryFirst {
                equalTo("id", movieIdFromExtra)
            }.also {
                withContext(Dispatchers.Main) {
                    if (it == null) finish()
                    _movieTitle.text = it?.title
                    _backdrop.setImageURI("https://image.tmdb.org/t/p/original${it?.backdrop_path}")
                    _ratingBar.rating = it?.vote_average ?: 0f
                    _movieOverview.text = it?.overview
                    _voteNb.text = (_voteNb.text.toString() + it?.vote_count.toString())
                }
            }
        }
    }
}
