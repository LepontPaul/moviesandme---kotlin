package com.example.moviesandme.models

import io.realm.RealmObject

open class SearchInput: RealmObject(){
    var id: Int = 0
    var userInput: String = ""
}