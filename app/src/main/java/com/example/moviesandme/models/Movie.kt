package com.example.moviesandme.models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Movie: RealmObject() {
    @PrimaryKey
    var id: Int = 0
    var title: String? = null
    var overview: String? = null
    var vote_average: Float? = null
    var vote_count: Int? = null
    var poster_path: String? = null
    var backdrop_path: String? = null
    var genres: RealmList<Genre> = RealmList()
    var isInFavorites: Boolean = false

    override fun toString(): String {
        return title.toString()
    }
}
