package com.example.moviesandme.ui.search

import android.os.Bundle
import android.system.Os.bind
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviesandme.adapters.MoviesAdapter
import com.example.moviesandme.adapters.SearchAdapter
import com.example.moviesandme.api.RetrofitHolder
import com.example.moviesandme.api.services.MovieService
import com.example.moviesandme.models.Movie
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search._recyclerView
import kotlinx.android.synthetic.main.simple_string_list_item.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.await


class SearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(com.example.moviesandme.R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val movieService: MovieService = RetrofitHolder.retrofit.create(MovieService::class.java)

        _searchResult.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val newInput = _searchResult.text.toString()
                var page = 1

                if (newInput.isNotBlank()) {
                    movieService.getSearchMovie(newInput, page).also {
                        GlobalScope.launch {
                            it.await().results.also {
                                withContext(Dispatchers.Main) {
                                    _recyclerView.apply {
                                        layoutManager = GridLayoutManager(context, 2)
                                        adapter = SearchAdapter(it)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

    }
}