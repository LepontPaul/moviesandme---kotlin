package com.example.moviesandme.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviesandme.R
import com.example.moviesandme.adapters.MoviesAdapter
import com.example.moviesandme.api.RetrofitHolder.retrofit
import com.example.moviesandme.api.services.MovieService
import com.example.moviesandme.models.Movie
import com.vicpin.krealmextensions.query
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.await

class NewsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_news, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val movieService: MovieService = retrofit.create(MovieService::class.java)

        movieService.getLastMovies().also {
            GlobalScope.launch {
                it.await().results.also {
                    val moviesInDb = Movie().query {
                        equalTo("isInFavorites", true)
                    }.map {
                        it.id
                    }
                    withContext(Dispatchers.Main) {
                        _recyclerView.apply {
                            layoutManager = GridLayoutManager(context, 2)
                            adapter = MoviesAdapter(it.map {
                                it.apply {
                                    isInFavorites = moviesInDb.contains(id)
                                }
                            })
                        }
                    }
                }
            }
        }

    }


}