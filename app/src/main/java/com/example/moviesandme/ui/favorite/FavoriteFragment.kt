package com.example.moviesandme.ui.favorite

import android.os.Bundle
import android.system.Os.bind
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviesandme.R
import com.example.moviesandme.adapters.FavoriteAdapter
import com.example.moviesandme.adapters.MoviesAdapter
import com.example.moviesandme.api.RetrofitHolder
import com.example.moviesandme.api.services.MovieService
import com.example.moviesandme.models.Movie
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.await

class FavoriteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_favorite, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //val movieService: MovieService = RetrofitHolder.retrofit.create(MovieService::class.java)

        //movieService.getLastMovies().also {
        GlobalScope.launch {
            //it.await().results.also {
            val moviesInDb = Movie().query {
                equalTo("isInFavorites", true)
            }
            withContext(Dispatchers.Main) {
                _recyclerView.apply {
                    layoutManager = GridLayoutManager(context, 3)
                    adapter = FavoriteAdapter(moviesInDb.toMutableList())
                }
            }
            //}
        }
        //}

    }
}