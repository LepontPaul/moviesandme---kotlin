package com.example.moviesandme.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.core.content.ContextCompat
import com.ramotion.paperonboarding.PaperOnboardingPage
import com.ramotion.paperonboarding.R
import com.ramotion.paperonboarding.listeners.*
import com.ramotion.paperonboarding.utils.PaperOnboardingEngineDefaults
import java.util.ArrayList

class MyPaperOnboardingEngine(
    rootLayout: View,
    contentElements: ArrayList<PaperOnboardingPage>?,
    appContext: Context
) :
    PaperOnboardingEngineDefaults {
    private val dpToPixelsScaleFactor: Float
    private val mRootLayout: RelativeLayout
    private val mContentTextContainer: FrameLayout
    private val mContentIconContainer: FrameLayout
    private val mBackgroundContainer: FrameLayout
    private val mPagerIconsContainer: LinearLayout
    private val mContentRootLayout: RelativeLayout
    private val mContentCenteredContainer: LinearLayout
    private val mAppContext: Context
    private val mElements = ArrayList<PaperOnboardingPage>()
    var activeElementIndex = 0
        private set
    private var mPagerElementActiveSize: Int = 0
    private var mPagerElementNormalSize: Int = 0
    private var mPagerElementLeftMargin: Int = 0
    private var mPagerElementRightMargin: Int = 0
    private var mOnChangeListener: PaperOnboardingOnChangeListener? = null
    private var mOnRightOutListener: PaperOnboardingOnRightOutListener? = null
    private var mOnLeftOutListener: PaperOnboardingOnLeftOutListener? = null

    protected val activeElement: PaperOnboardingPage?
        get() =
            if (this.mElements.size > this.activeElementIndex) this.mElements.get(this.activeElementIndex) else null

    init {
        if (contentElements != null && !contentElements.isEmpty()) {
            this.mElements.addAll(contentElements)
            this.mAppContext = appContext.applicationContext
            this.mRootLayout = rootLayout as RelativeLayout
            this.mContentTextContainer =
                rootLayout.findViewById<View>(R.id.onboardingContentTextContainer) as FrameLayout
            this.mContentIconContainer =
                rootLayout.findViewById<View>(R.id.onboardingContentIconContainer) as FrameLayout
            this.mBackgroundContainer =
                rootLayout.findViewById<View>(R.id.onboardingBackgroundContainer) as FrameLayout
            this.mPagerIconsContainer =
                rootLayout.findViewById<View>(R.id.onboardingPagerIconsContainer) as LinearLayout
            this.mContentRootLayout = this.mRootLayout.getChildAt(1) as RelativeLayout
            this.mContentCenteredContainer = this.mContentRootLayout.getChildAt(0) as LinearLayout
            this.dpToPixelsScaleFactor = this.mAppContext.resources.displayMetrics.density
            this.initializeStartingState()
            this.mRootLayout.setOnTouchListener(object : OnSwipeListener(this.mAppContext) {
                override fun onSwipeLeft() {
                    this@MyPaperOnboardingEngine.toggleContent(false)
                }

                override fun onSwipeRight() {
                    this@MyPaperOnboardingEngine.toggleContent(true)
                }
            })
            this.mRootLayout.viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (Build.VERSION.SDK_INT >= 16) {
                        this@MyPaperOnboardingEngine.mRootLayout.viewTreeObserver.removeOnGlobalLayoutListener(
                            this
                        )
                    } else {
                        this@MyPaperOnboardingEngine.mRootLayout.viewTreeObserver.removeGlobalOnLayoutListener(
                            this
                        )
                    }

                    this@MyPaperOnboardingEngine.mPagerElementActiveSize =
                        this@MyPaperOnboardingEngine.mPagerIconsContainer.height
                    this@MyPaperOnboardingEngine.mPagerElementNormalSize = Math.min(
                        this@MyPaperOnboardingEngine.mPagerIconsContainer.getChildAt(0).height,
                        this@MyPaperOnboardingEngine.mPagerIconsContainer.getChildAt(this@MyPaperOnboardingEngine.mPagerIconsContainer.childCount - 1).height
                    )
                    val layoutParams =
                        this@MyPaperOnboardingEngine.mPagerIconsContainer.getChildAt(0).layoutParams as ViewGroup.MarginLayoutParams
                    this@MyPaperOnboardingEngine.mPagerElementLeftMargin = layoutParams.leftMargin
                    this@MyPaperOnboardingEngine.mPagerElementRightMargin = layoutParams.rightMargin
                    this@MyPaperOnboardingEngine.mPagerIconsContainer.x =
                        this@MyPaperOnboardingEngine.calculateNewPagerPosition(0).toFloat()
                    this@MyPaperOnboardingEngine.mContentCenteredContainer.y =
                        ((this@MyPaperOnboardingEngine.mContentRootLayout.height - this@MyPaperOnboardingEngine.mContentCenteredContainer.height) / 2).toFloat()
                }
            })
        } else {
            throw IllegalArgumentException("No content elements provided")
        }
    }

    protected fun calculateNewPagerPosition(newActiveElement: Int): Int {
        var newActiveElement = newActiveElement
        ++newActiveElement
        if (newActiveElement <= 0) {
            newActiveElement = 1
        }

        val pagerActiveElemCenterPosX =
            this.mPagerElementActiveSize / 2 + newActiveElement * this.mPagerElementLeftMargin + (newActiveElement - 1) * (this.mPagerElementNormalSize + this.mPagerElementRightMargin)
        return this.mRootLayout.width / 2 - pagerActiveElemCenterPosX
    }

    protected fun calculateCurrentCenterCoordinatesOfPagerElement(activeElementIndex: Int): IntArray {
        val y =
            (this.mPagerIconsContainer.y + (this.mPagerIconsContainer.height / 2).toFloat()).toInt()
        if (activeElementIndex >= this.mPagerIconsContainer.childCount) {
            return intArrayOf(this.mRootLayout.width / 2, y)
        } else {
            val pagerElem = this.mPagerIconsContainer.getChildAt(activeElementIndex)
            val x =
                (this.mPagerIconsContainer.x + pagerElem.x + (pagerElem.width / 2).toFloat()).toInt()
            return intArrayOf(x, y)
        }
    }

    protected fun initializeStartingState() {
        for (i in this.mElements.indices) {
            val PaperOnboardingPage = this.mElements.get(i) as PaperOnboardingPage
            val bottomBarIconElement =
                this.createPagerIconElement(PaperOnboardingPage.bottomBarIconRes, i == 0)
            this.mPagerIconsContainer.addView(bottomBarIconElement)
        }

        val activeElement = this.activeElement
        val initialContentText = this.createContentTextView(activeElement!!)
        this.mContentTextContainer.addView(initialContentText)
        val initContentIcon = this.createContentIconView(activeElement)
        this.mContentIconContainer.addView(initContentIcon)
        this.mRootLayout.setBackgroundColor(activeElement.bgColor)
    }

    protected fun toggleContent(prev: Boolean) {
        val oldElementIndex = this.activeElementIndex
        val newElement = if (prev) this.toggleToPreviousElement() else this.toggleToNextElement()
        if (newElement == null) {
            if (prev && this.mOnLeftOutListener != null) {
                this.mOnLeftOutListener!!.onLeftOut()
            }

            if (!prev && this.mOnRightOutListener != null) {
                this.mOnRightOutListener!!.onRightOut()
            }

        } else {
            val newPagerPosX = this.calculateNewPagerPosition(this.activeElementIndex)
            val bgAnimation = this.createBGAnimatorSet(newElement.bgColor)
            val pagerMoveAnimation = ObjectAnimator.ofFloat(
                this.mPagerIconsContainer,
                "x",
                *floatArrayOf(this.mPagerIconsContainer.x, newPagerPosX.toFloat())
            )
            pagerMoveAnimation.duration = 700L
            val pagerIconAnimation =
                this.createPagerIconAnimation(oldElementIndex, this.activeElementIndex)
            val newContentText = this.createContentTextView(newElement)
            this.mContentTextContainer.addView(newContentText)
            val contentTextShowAnimation = this.createContentTextShowAnimation(
                this.mContentTextContainer.getChildAt(this.mContentTextContainer.childCount - 2),
                newContentText
            )
            val newContentIcon = this.createContentIconView(newElement)
            this.mContentIconContainer.addView(newContentIcon)
            val contentIconShowAnimation = this.createContentIconShowAnimation(
                this.mContentIconContainer.getChildAt(this.mContentIconContainer.childCount - 2),
                newContentIcon
            )
            val centerContentAnimation =
                this.createContentCenteringVerticalAnimation(newContentText, newContentIcon)
            centerContentAnimation.start()
            bgAnimation.start()
            pagerMoveAnimation.start()
            pagerIconAnimation.start()
            contentIconShowAnimation.start()
            contentTextShowAnimation.start()
            if (this.mOnChangeListener != null) {
                this.mOnChangeListener!!.onPageChanged(oldElementIndex, this.activeElementIndex)
            }

        }
    }

    fun setOnChangeListener(onChangeListener: PaperOnboardingOnChangeListener?) {
        this.mOnChangeListener = onChangeListener
    }

    fun setOnRightOutListener(onRightOutListener: PaperOnboardingOnRightOutListener?) {
        this.mOnRightOutListener = onRightOutListener
    }

    fun setOnLeftOutListener(onLeftOutListener: PaperOnboardingOnLeftOutListener?) {
        this.mOnLeftOutListener = onLeftOutListener
    }

    protected fun createBGAnimatorSet(color: Int): AnimatorSet {
        val bgColorView = ImageView(this.mAppContext)
        bgColorView.layoutParams = RelativeLayout.LayoutParams(
            this.mRootLayout.width,
            this.mRootLayout.height
        )
        bgColorView.setBackgroundColor(color)
        this.mBackgroundContainer.addView(bgColorView)
        val pos = this.calculateCurrentCenterCoordinatesOfPagerElement(this.activeElementIndex)
        val finalRadius =
            if (this.mRootLayout.width > this.mRootLayout.height) this.mRootLayout.width.toFloat() else this.mRootLayout.height.toFloat()
        val bgAnimSet = AnimatorSet()
        val fadeIn = ObjectAnimator.ofFloat(bgColorView, "alpha", *floatArrayOf(0.0f, 1.0f))
        if (Build.VERSION.SDK_INT >= 21) {
            val circularReveal = ViewAnimationUtils.createCircularReveal(
                bgColorView,
                pos[0],
                pos[1],
                0.0f,
                finalRadius
            )
            circularReveal.interpolator = AccelerateInterpolator()
            bgAnimSet.playTogether(*arrayOf(circularReveal, fadeIn))
        } else {
            bgAnimSet.playTogether(*arrayOf<Animator>(fadeIn))
        }

        bgAnimSet.duration = 450L
        bgAnimSet.addListener(object : AnimatorEndListener() {
            override fun onAnimationEnd(animation: Animator) {
                this@MyPaperOnboardingEngine.mRootLayout.setBackgroundColor(color)
                bgColorView.visibility = View.GONE

                this@MyPaperOnboardingEngine.mBackgroundContainer.removeView(bgColorView)
            }
        })
        return bgAnimSet
    }

    private fun createContentTextShowAnimation(
        currentContentText: View,
        newContentText: View
    ): AnimatorSet {
        val positionDeltaPx = this.dpToPixels(50)
        val animations = AnimatorSet()
        val currentContentMoveUp = ObjectAnimator.ofFloat(
            currentContentText,
            "y",
            *floatArrayOf(0.0f, (-positionDeltaPx).toFloat())
        )
        currentContentMoveUp.duration = 200L
        currentContentMoveUp.addListener(object : AnimatorEndListener() {
            override fun onAnimationEnd(animation: Animator) {
                this@MyPaperOnboardingEngine.mContentTextContainer.removeView(currentContentText)
            }
        })
        val currentContentFadeOut =
            ObjectAnimator.ofFloat(currentContentText, "alpha", *floatArrayOf(1.0f, 0.0f))
        currentContentFadeOut.duration = 200L
        animations.playTogether(*arrayOf<Animator>(currentContentMoveUp, currentContentFadeOut))
        val newContentMoveUp = ObjectAnimator.ofFloat(
            newContentText,
            "y",
            *floatArrayOf(positionDeltaPx.toFloat(), 0.0f)
        )
        newContentMoveUp.duration = 800L
        val newContentFadeIn =
            ObjectAnimator.ofFloat(newContentText, "alpha", *floatArrayOf(0.0f, 1.0f))
        newContentFadeIn.duration = 800L
        animations.playTogether(*arrayOf<Animator>(newContentMoveUp, newContentFadeIn))
        animations.interpolator = DecelerateInterpolator()
        return animations
    }

    protected fun createContentIconShowAnimation(
        currentContentIcon: View,
        newContentIcon: View
    ): AnimatorSet {
        val positionDeltaPx = this.dpToPixels(50)
        val animations = AnimatorSet()
        val currentContentMoveUp = ObjectAnimator.ofFloat(
            currentContentIcon,
            "y",
            *floatArrayOf(0.0f, (-positionDeltaPx).toFloat())
        )
        currentContentMoveUp.duration = 200L
        currentContentMoveUp.addListener(object : AnimatorEndListener() {
            override fun onAnimationEnd(animation: Animator) {
                this@MyPaperOnboardingEngine.mContentIconContainer.removeView(currentContentIcon)
            }
        })
        val currentContentFadeOut =
            ObjectAnimator.ofFloat(currentContentIcon, "alpha", *floatArrayOf(1.0f, 0.0f))
        currentContentFadeOut.duration = 200L
        animations.playTogether(*arrayOf<Animator>(currentContentMoveUp, currentContentFadeOut))
        val newContentMoveUp = ObjectAnimator.ofFloat(
            newContentIcon,
            "y",
            *floatArrayOf(positionDeltaPx.toFloat(), 0.0f)
        )
        newContentMoveUp.duration = 800L
        val newContentFadeIn =
            ObjectAnimator.ofFloat(newContentIcon, "alpha", *floatArrayOf(0.0f, 1.0f))
        newContentFadeIn.duration = 800L
        animations.playTogether(*arrayOf<Animator>(newContentMoveUp, newContentFadeIn))
        animations.interpolator = DecelerateInterpolator()
        return animations
    }

    protected fun createContentCenteringVerticalAnimation(
        newContentText: View,
        newContentIcon: View
    ): Animator {
        newContentText.measure(
            View.MeasureSpec.makeMeasureSpec(
                this.mContentCenteredContainer.width,
                View.MeasureSpec.UNSPECIFIED
            ), -2
        )

        val measuredContentTextHeight = newContentText.measuredHeight
        newContentIcon.measure(-2, -2)
        val measuredContentIconHeight = newContentIcon.measuredHeight
        val newHeightOfContent =
            measuredContentIconHeight + measuredContentTextHeight + (this.mContentTextContainer.layoutParams as ViewGroup.MarginLayoutParams).topMargin
        val centerContentAnimation = ObjectAnimator.ofFloat(
            this.mContentCenteredContainer,
            "y",
            *floatArrayOf(
                this.mContentCenteredContainer.y,
                ((this.mContentRootLayout.height - newHeightOfContent) / 2).toFloat()
            )
        )
        centerContentAnimation.duration = 800L
        centerContentAnimation.interpolator = DecelerateInterpolator()
        return centerContentAnimation
    }

    protected fun createPagerIconAnimation(oldIndex: Int, newIndex: Int): AnimatorSet {
        val animations = AnimatorSet()
        animations.duration = 350L
        val oldActiveItem = this.mPagerIconsContainer.getChildAt(oldIndex) as ViewGroup
        val oldActiveItemParams =
            oldActiveItem.layoutParams as android.widget.LinearLayout.LayoutParams
        val oldItemScaleDown = ValueAnimator.ofInt(
            *intArrayOf(
                this.mPagerElementActiveSize,
                this.mPagerElementNormalSize
            )
        )
        oldItemScaleDown.addUpdateListener { valueAnimator ->
            oldActiveItemParams.height = valueAnimator.animatedValue as Int
            oldActiveItemParams.width = valueAnimator.animatedValue as Int
            oldActiveItem.requestLayout()
        }
        val oldActiveIcon = oldActiveItem.getChildAt(1)
        val oldActiveIconFadeOut =
            ObjectAnimator.ofFloat(oldActiveIcon, "alpha", *floatArrayOf(1.0f, 0.0f))
        val oldActiveShape = oldActiveItem.getChildAt(0) as ImageView
        oldActiveShape.setImageResource(if (oldIndex - newIndex > 0) R.drawable.onboarding_pager_circle_icon else R.drawable.onboarding_pager_round_icon)
        val oldActiveShapeFadeIn =
            ObjectAnimator.ofFloat(oldActiveShape, "alpha", *floatArrayOf(0.0f, 0.5f))
        animations.playTogether(
            *arrayOf<Animator>(
                oldItemScaleDown,
                oldActiveIconFadeOut,
                oldActiveShapeFadeIn
            )
        )
        val newActiveItem = this.mPagerIconsContainer.getChildAt(newIndex) as ViewGroup
        val newActiveItemParams =
            newActiveItem.layoutParams as android.widget.LinearLayout.LayoutParams
        val newItemScaleUp = ValueAnimator.ofInt(
            *intArrayOf(
                this.mPagerElementNormalSize,
                this.mPagerElementActiveSize
            )
        )
        newItemScaleUp.addUpdateListener { valueAnimator ->
            newActiveItemParams.height = valueAnimator.animatedValue as Int
            newActiveItemParams.width = valueAnimator.animatedValue as Int
            newActiveItem.requestLayout()
        }
        val newActiveIcon = newActiveItem.getChildAt(1)
        val newActiveIconFadeIn =
            ObjectAnimator.ofFloat(newActiveIcon, "alpha", *floatArrayOf(0.0f, 1.0f))
        val newActiveShape = newActiveItem.getChildAt(0) as ImageView
        val newActiveShapeFadeOut =
            ObjectAnimator.ofFloat(newActiveShape, "alpha", *floatArrayOf(0.5f, 0.0f))
        animations.playTogether(
            *arrayOf<Animator>(
                newItemScaleUp,
                newActiveShapeFadeOut,
                newActiveIconFadeIn
            )
        )
        animations.interpolator = DecelerateInterpolator()
        return animations
    }

    protected fun createPagerIconElement(iconDrawableRes: Int, isActive: Boolean): ViewGroup {
        val vi = LayoutInflater.from(this.mAppContext)
        val bottomBarElement = vi.inflate(
            R.layout.onboarding_pager_layout,
            this.mPagerIconsContainer,
            false
        ) as FrameLayout
        val elementShape = bottomBarElement.getChildAt(0) as ImageView
        val elementIcon = bottomBarElement.getChildAt(1) as ImageView
        elementIcon.setImageResource(iconDrawableRes)
        if (isActive) {
            val layoutParams =
                bottomBarElement.layoutParams as android.widget.LinearLayout.LayoutParams
            layoutParams.width = this.mPagerIconsContainer.layoutParams.height
            layoutParams.height = this.mPagerIconsContainer.layoutParams.height
            elementShape.alpha = 0.0f
            elementIcon.alpha = 1.0f
        } else {
            elementShape.alpha = 0.5f
            elementIcon.alpha = 0.0f
        }

        return bottomBarElement
    }

    protected fun createContentTextView(PaperOnboardingPage: PaperOnboardingPage): ViewGroup {
        val vi = LayoutInflater.from(this.mAppContext)
        val contentTextView = vi.inflate(
            R.layout.onboarding_text_content_layout,
            this.mContentTextContainer,
            false
        ) as ViewGroup
        val contentTitle = contentTextView.getChildAt(0) as TextView


        contentTitle.setTextColor(Color.parseColor("#EEEEEE"))
        contentTitle.text = PaperOnboardingPage.titleText
        val contentText = contentTextView.getChildAt(1) as TextView

        contentText.setTextColor(Color.parseColor("#EEEEEE"))
        contentText.text = PaperOnboardingPage.descriptionText
        return contentTextView
    }

    protected fun createContentIconView(PaperOnboardingPage: PaperOnboardingPage): ImageView {
        val contentIcon = ImageView(this.mAppContext)
        contentIcon.setImageResource(PaperOnboardingPage.contentIconRes)
        val iconLP = android.widget.FrameLayout.LayoutParams(-2, -2)
        iconLP.gravity = 17
        contentIcon.layoutParams = iconLP
        return contentIcon
    }

    protected fun toggleToPreviousElement(): PaperOnboardingPage? {
        if (this.activeElementIndex - 1 >= 0) {
            --this.activeElementIndex
            return if (this.mElements.size > this.activeElementIndex) this.mElements.get(this.activeElementIndex) else null
        } else {
            return null
        }
    }

    protected fun toggleToNextElement(): PaperOnboardingPage? {
        if (this.activeElementIndex + 1 < this.mElements.size) {
            ++this.activeElementIndex
            return if (this.mElements.size > this.activeElementIndex) this.mElements.get(this.activeElementIndex) else null
        } else {
            return null
        }
    }

    protected fun dpToPixels(dpValue: Int): Int {
        return (dpValue.toFloat() * this.dpToPixelsScaleFactor + 0.5f).toInt()
    }
}
