package com.example.moviesandme.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.*
import com.ramotion.paperonboarding.R
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ramotion.paperonboarding.PaperOnboardingEngine
import com.ramotion.paperonboarding.PaperOnboardingFragment
import com.ramotion.paperonboarding.PaperOnboardingPage
import com.ramotion.paperonboarding.listeners.*
import com.ramotion.paperonboarding.utils.PaperOnboardingEngineDefaults
import java.util.ArrayList


class MyPaperOnboardingFragment : Fragment() {
    private var mOnChangeListener: PaperOnboardingOnChangeListener? = null
    private var mOnRightOutListener: PaperOnboardingOnRightOutListener? = null
    private var mOnLeftOutListener: PaperOnboardingOnLeftOutListener? = null
    var elements: ArrayList<PaperOnboardingPage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (this.arguments != null) {
            this.elements = this.arguments?.get("elements") as ArrayList<PaperOnboardingPage>
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.onboarding_main_layout, container, false)
        val mPaperOnboardingEngine = MyPaperOnboardingEngine(
            view.findViewById(R.id.onboardingRootView),
            this.elements!!,
            this.activity!!.applicationContext
        )
        mPaperOnboardingEngine.setOnChangeListener(this.mOnChangeListener)
        mPaperOnboardingEngine.setOnLeftOutListener(this.mOnLeftOutListener)
        mPaperOnboardingEngine.setOnRightOutListener(this.mOnRightOutListener)
        return view
    }

    fun setOnChangeListener(onChangeListener: PaperOnboardingOnChangeListener?) {
        this.mOnChangeListener = onChangeListener
    }

    fun setOnRightOutListener(onRightOutListener: PaperOnboardingOnRightOutListener?) {
        this.mOnRightOutListener = onRightOutListener
    }

    fun setOnLeftOutListener(onLeftOutListener: PaperOnboardingOnLeftOutListener?) {
        this.mOnLeftOutListener = onLeftOutListener
    }

    companion object {
        private val ELEMENTS_PARAM = "elements"

        fun newInstance(elements: ArrayList<PaperOnboardingPage>): MyPaperOnboardingFragment {
            val fragment = MyPaperOnboardingFragment()
            val args = Bundle()
            args.putSerializable("elements", elements)
            fragment.arguments = args
            return fragment
        }
    }
}
